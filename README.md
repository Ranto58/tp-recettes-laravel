Bonjour,

Voici le tp-recettes en laravel.
pour le lancer, il faudra d'abord cloner le projet.
Ensuite il faut installer les dépendances avec la commande **composer install**.
Ensuite il faudra renseigner le nom de la base de données dans le fichier **.env**
Puis il faudra executer la commande **php artisan migrate:fresh** et la commande **php artisan db:seed**

Pour accéder à la page admin, il faudra se **register** au moment du login et pour avoir un compte permettant de s'authentifier.
Une fois connecté, vous pourrez créer, modifier et supprimer des recettes, des ingrédients et la catégorie"
Cordialement,

Rakotomalala Rantoniaina Antonio