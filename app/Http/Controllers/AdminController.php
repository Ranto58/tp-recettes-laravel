<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Category;
use App\Ingredient;
use App\Recipe;
use DB;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function recipes(){
        $recipes = DB::table('recipes')->get();
        return view('admin.recipes', ['recipes' => $recipes, 'mode' => 'edit']);
    }
    public function ingredients(){
        $ingredients = DB::table('ingredients')->get();
        return view('admin.ingredients', ['ingredients' => $ingredients, 'mode' => 'edit']);
    }
    public function categories(){
        $categories = DB::table('categories')->get();
        return view('admin.categories', ['categories' => $categories, 'mode' => 'edit']);
    }
}