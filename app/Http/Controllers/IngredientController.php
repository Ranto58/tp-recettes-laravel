<?php

namespace App\Http\Controllers;

use App\Ingredient;

use Illuminate\Http\Request;
use DB; 

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ingredients = DB::table('ingredients')->get();
        return view('admin.ingredients', ['ingredients' => $ingredients, 'mode' => 'edit']);

    }
    public function normal()
    {
        $ingredients = DB::table('ingredients')->get();
        return view('ingredients.index', ['ingredients' => $ingredients, 'mode' => 'normal']);
    }

    public function search()
    {
        $q = "whatever";
        $user = User::where('name', 'LIKE', '%' . $q . '%')->orWhere('email', 'LIKE', '%' . $q . '%')->get();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       

        return view('ingredients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $inputs = $request->except('_token');

        $ingredient = new Ingredient();

        $ingredient->name = $request->input('name');
        $ingredient->description = $request->input('description');
        $ingredient->save();

        return redirect('admin/ingredients');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function show(Ingredient $ingredient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ingredient = Ingredient::find($id);
        return view('ingredients.edit', ['ingredient' => $ingredient]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $inputs = $request->except('_token', '_method');
        $ingredient = Ingredient::find($id);
        foreach($inputs as $key => $value){
            $ingredient->$key = $value;
        }
        $ingredient->save();
        return redirect('admin/ingredients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $ingredient = Ingredient::find($id);
        $ingredient->delete();
        return redirect('admin/ingredients');
    }

    // public function recipe($id){
    //     $recipe = Recipe::find($id)->recipe()->name;
    // }
}
