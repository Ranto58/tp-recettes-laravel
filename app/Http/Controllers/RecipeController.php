<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\Ingredient;
use DB;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $recipes = DB::table('recipes')->get();
        $recipes = Recipe::with('ingredients')->get();
        // return $recipes;
        // return $recipes;
        return view('admin.recipes', ['recipes' => $recipes, 'mode' => 'edit']);

    }
    public function normal()
    {
        // $recipes = DB::table('recipes')->get();
        $recipes = Recipe::with('ingredients')->get();
        return view('recipes.index', ['recipes' => $recipes, 'mode' => 'normal']);
    }

    public function search()
    {
        $q = "whatever";
        $user = User::where('name', 'LIKE', '%' . $q . '%')->orWhere('email', 'LIKE', '%' . $q . '%')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $ingredients = Ingredient::all();
        return view('recipes.create', ['ingredients' => $ingredients]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $recipe = new Recipe();
        $recipe->name = $request->input('name');
        $recipe->description = $request->input('description');
        $recipe->save();

        $ingredients = Ingredient::find($request->input('ingredients'));
        $recipe->ingredients()->attach($ingredients);
        return redirect('admin/recipes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        //
        return view('recipes.show', compact('recipes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $recipe = Recipe::find($id);
        return view('recipes.edit', ['recipe' => $recipe]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $inputs = $request->except('_token', '_method');
        $recipe = Recipe::find($id);
        foreach ($inputs as $key => $value) {
            $recipe->$key = $value;
        }
        $recipe->save();
        return redirect('admin/recipes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $recipe = Recipe::find($id);
        $recipe->delete();
        return redirect('admin/recipes');
    }
    // public function ingredient()
    // {
    //     $ingredients = User::first()->ingredients(); 
    //     foreach ($ingredients as $ingredient) {
    //         echo $ingredient->name;
    //     }

    // }

}
