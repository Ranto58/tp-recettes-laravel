<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    //
    // public function ingredients(){
    //     return $this->belongsToMany('Ingredient');
    // }   

    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'recipe_ingredient');
    }
        public function category(){
        return $this->belongsTo(Category::class);
    }
}
