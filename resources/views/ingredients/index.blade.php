@extends('layouts.template')
@section('title', 'Mes ingrédients')
@section('content')
@component('components.ingredients', ['ingredients' => $ingredients, 'mode' => 'normal'])
@endcomponent
@endsection