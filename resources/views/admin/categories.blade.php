@extends('admin.template')

@section('title', 'Mes categories')

@section('content')
@component('components.categories', ['categories' => $categories, 'mode' => 'edit'])
@endcomponent
@endsection