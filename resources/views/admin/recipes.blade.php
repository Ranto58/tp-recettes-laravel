@extends('admin.template')

@section('title', 'Mes recettes')

@section('content')
@component('components.recipes', ['recipes' => $recipes, 'mode' => 'edit'])
@endcomponent
@endsection