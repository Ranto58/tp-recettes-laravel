@extends('admin.template')

@section('title', 'Mes ingredients')

@section('content')
@component('components.ingredients', ['ingredients' => $ingredients, 'mode' => 'edit'])
@endcomponent
@endsection