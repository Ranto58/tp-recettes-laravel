@extends('layouts.template')
@section('title', 'Mes catégories')
@section('content')
@component('components.categories', ['categories' => $categories, 'mode' => 'normal'])
@endcomponent
@endsection