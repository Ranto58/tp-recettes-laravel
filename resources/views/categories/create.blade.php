@extends('admin.template')

@section('title', 'Créer Categories')

@section('content')

<form class="ml-3 mr-3" method="POST" action="{{route('Categories.store')}}">
    @csrf
    <div class="form-group">
        <label for="name">name</label>
        <textarea type="name" name="name" class="form-control" id="name"></textarea>
    </div>
    <div class="form-group">
        <label for="description">description</label>
        <textarea type="description" name="description" class="form-control" id="description"></textarea>
    </div>
    <button type="submit" class="btn btn-primary" style="margin-left:50%;">créer</button>
    @endsection