@extends('admin.template')

@section('title', 'Modifier - Recettes')

@section('content')
<form class="ml-3 mr-3" method="POST" action="{{route('recipes.update', ['recipe' => $recipe->id])}}">
@method('PUT')
@csrf
<div class="row">
<div class="form-group">
    <label for="name">name</label>
    <textarea type="name" name="name" class="form-control" id="name">{{$recipe->name}}</textarea>
  </div>
  <div class="form-group">
    <label for="description">description</label>
    <textarea type="description" name="description" class="form-control" id="description">{{$recipe->description}}</textarea>
  </div>
</div>
{{-- <div class="form-group">
  <label for="exampleFormControlSelect1">Ingredient</label>
  <select class="form-control" name="ingredient">
    <option>{{$recipe->ingredient}}</option>

  </select>
</div> --}}
  <button type="submit" class="btn btn-primary" style="margin-left:50%;">Mettre à jour</button>
</form>
@endsection
