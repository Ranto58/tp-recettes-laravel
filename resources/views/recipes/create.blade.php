@extends('admin.template')

@section('title', 'Créer Recettes')

@section('content')

<form class="ml-3 mr-3" method="POST" action="{{route('recipes.store')}}">
  @csrf
  <div class="form-group">
    <label for="name">name</label>
    <textarea type="name" name="name" class="form-control" id="name"></textarea>
  </div>
  <div class="form-group">
    <label for="description">description</label>
    <textarea type="description" name="description" class="form-control" id="description"></textarea>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect2">Example multiple select</label>
    <select multiple class="form-control" id="exampleFormControlSelect2" name="ingredients[]">
      @foreach($ingredients as $ingredient)
      <option value={{$ingredient->id}}>{{$ingredient->name}}</option>

      @endforeach
    </select>
  </div>
  <button type="submit" class="btn btn-primary" style="margin-left:50%;">créer</button>
  @endsection