@extends('layouts.template')
@section('title', 'Mes Recettes')
@section('content')
@component('components.recipes', ['recipes' => $recipes, 'mode' => 'normal'])
@endcomponent
@endsection