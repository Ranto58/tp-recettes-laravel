@foreach($recipes as $recipe)

<div class="container mt-4">
    <div class="row  mb-2">
        <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$recipe->name}}</div>
        <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$recipe->description}}</div>
        {{-- <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$recipe->ingredient}}
    </div> --}}
    <button type="button" class="btn btn-primary" data-toggle="modal"
        data-target=".bd-example-modal-sm-{{ $recipe->id }}">Details</button>

    <div class="modal fade bd-example-modal-sm-{{ $recipe->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                {{$recipe->name}}
                <hr>
                <ul>
                    @foreach ($recipe->ingredients as $ingredient)
                    <li>{{ $ingredient->name }} - {{ $ingredient->description }}</li>         
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @if($mode == 'edit')
    <div class="row d-flex ml-2" style="height: 40px; margin-top:55px;">
        <a class="btn btn-primary" href="{{route('recipes.edit', ['recipe' => $recipe->id])}}" role="button">
            <i class="fas fa-pen"></i>
        </a>
    </div>
    <form method="POST" action="{{route('recipes.destroy', ['recipe' => $recipe->id])}}">
        @method('DELETE') @csrf
        <div class="row  ml-4" style="height: 40px; margin-top:55px;">
            <button type="submit" class="btn btn-primary">
                <i class="fas fa-trash"></i>
            </button>
        </div>
    </form>
    @endif
</div>

</div>
@endforeach
@if($mode == 'edit')
<div class="row">
    <div class="mx-auto">
        <a class="btn btn-primary" href="{{route('recipes.create')}}" role="button">
            <i class="fas fa-plus-circle"></i>
        </a>
    </div>
</div>
@endif