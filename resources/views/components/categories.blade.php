@foreach($categories as $category)

<div class="container mt-4">
    <div class="row  mb-2">
        <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$category->name}}</div>
        <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$category->description}}</div>
        @if($mode == 'edit')
        <div class="row d-flex ml-2" style="height: 40px; margin-top:55px;">
            <a class="btn btn-primary" href="{{route('Categories.edit', ['Category' => $category->id])}}"
                role="button">
                <i class="fas fa-pen"></i>
            </a>
        </div>
        <form method="POST" action="{{route('Categories.destroy', ['Category' => $category->id])}}">
            @method('DELETE') @csrf
            <div class="row  ml-4" style="height: 40px; margin-top:55px;">
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </form>
        @endif
    </div>

</div>
@endforeach
@if($mode == 'edit')
<div class="row">
    <div class="mx-auto">
        <a class="btn btn-primary" href="{{route('Categories.create')}}" role="button">
            <i class="fas fa-plus-circle"></i>
        </a>
    </div>
</div>
@endif