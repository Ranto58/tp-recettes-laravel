<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'RecipeController@normal');

Route::get('/recipes', 'RecipeController@normal');

Auth::routes();
Route::middleware('auth')->group(function (){
    Route::get('admin/recipes', 'AdminController@recipes');
    Route::get('admin/ingredients', 'AdminController@ingredients');
    Route::get('admin/categories', 'AdminController@categories');
    Route::resources([
        'admin/recipes' => 'RecipeController',
        'admin/ingredients' => 'IngredientController',
        'admin/Categories' => 'CategoryController',
    ]);
});
Route::get('/home', 'HomeController@index')->name('home');

