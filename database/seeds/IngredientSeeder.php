<?php

use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('ingredients')->insert(
            array(
                array(
                    'name' => 'tomates',
                    'description' => 'fruits rouges',
                ),
            )
        );
    }
}
