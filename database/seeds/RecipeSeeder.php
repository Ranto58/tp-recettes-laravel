<?php

use Illuminate\Database\Seeder;

class RecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('recipes')->insert(
            array(
                array(
                    'name' => 'Madeleine aux framboises',
                    'description' => 'Madeleine sucré avec des framboises à l\'intérieur',
                ),
            )
        );
    }
}
